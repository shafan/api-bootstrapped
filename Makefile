# -- Inspired by -----------------------------------------------------------
# https://www.strangebuzz.com/fr/snippets/le-makefile-parfait-pour-symfony
# https://github.com/mykiwi/symfony-bootstrapped/blob/master/Makefile
# https://phpqa.io/


# Setup --------------------------------------------------------------------
SYMFONY_BIN		= ./symfony
EXEC_PHP		= $(SYMFONY_BIN) php
COMPOSER		= $(SYMFONY_BIN) composer
CONSOLE			= $(SYMFONY_BIN) console
DOCKER			= docker-compose
.DEFAULT_GOAL	= help

## -- The Shafan Symfony Makefile ------------------------------------------
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

wait: ## Sleep 5 seconds
	sleep 5

##
## Project
## -------------------------------------------------------------------------
##

build:
	@$(DOCKER) pull --quiet --ignore-pull-failures 2> /dev/null
	$(DOCKER) build --pull

kill: unserve
	$(DOCKER) kill
	$(DOCKER) down --volumes --remove-orphans

start: start-docker serve ## Start the project

start-docker: ##Start docker
	$(DOCKER) up -d --remove-orphans --no-recreate

stop: unserve ## Stop the project
	$(DOCKER) stop

install: ## Install and start the project
install: .env.local .env.test.local build auth bin-install cert-install start db .php_cs .phpmd.xml .phpcs.xml phpstan.neon psalm.xml phpunit.xml status

reset: ## Stop and start a fresh install of the project
reset: kill install

clean: ## Stop the project and remove generated files
clean: kill
	rm -rf .env.local .env.test.local vendor node_modules
	rm .php_cs .php_cs.cache .phpmd.xml .phpcs.xml phpstan.neon psalm.xml phpunit.xml
	rm config/jwt/private.pem config/jwt/public.pem
	rm symfony

.PHONY: build kill install reset start stop clean

## -- Symfony binary ----------------------------------------------------------
bin-install: ## Download and install the binary in the project (file is ignored)
	curl -sS https://get.symfony.com/cli/installer | bash
	mv ~/.symfony/bin/symfony .

cert-install: ## Install the local HTTPS certificates
	$(SYMFONY_BIN) server:ca:install

serve: ## Serve the application with HTTPS support
	$(SYMFONY_BIN) serve --daemon

unserve: ## Stop the web server
	$(SYMFONY_BIN) server:stop

status: ## Status of web server
	$(SYMFONY_BIN) server:status

.PHONY: bin-install cert-install serve unserve status

# L71+12 => templates/blog/posts/_64.html.twig

##
## Utils
## -----
##

db: ## Reset the database and load fixtures
db: .env.local .env.test.local vendor
	-$(CONSOLE) doctrine:schema:drop --full-database --force
	-$(CONSOLE) doctrine:schema:update  --force
	$(CONSOLE) doctrine:migrations:migrate --no-interaction --allow-no-migration
	$(CONSOLE) hautelook:fixtures:load --no-interaction --purge-with-truncate

.PHONY: db

# rules based on files
composer.lock: composer.json
	$(COMPOSER) update --lock --no-scripts --no-interaction

vendor: composer.lock
	$(COMPOSER) install

.env.local: .env.local.docker
	@if [ -f .env.local ]; \
	then\
		echo '\033[1;41m/!\ The .env.local.docker file has changed. Please check your .env.local file (this message will not be displayed again).\033[0m';\
		touch .env.local;\
	else\
		echo cp .env.local.docker .env.local;\
		cp .env.local.docker .env.local;\
	fi

.env.test.local: .env.test.local.docker
	@if [ -f .env.test.local ]; \
	then\
		echo '\033[1;41m/!\ The .env.test.local.docker file has changed. Please check your .env.test.local file (this message will not be displayed again).\033[0m';\
		touch .env.test.local;\
	else\
		echo cp .env.test.local.docker .env.test.local;\
		cp .env.test.local.docker .env.test.local;\
	fi
##
## Authentication
## --------------
##

auth:
	jwt_passphrase=$$(grep ''^JWT_PASSPHRASE='' .env.local | cut -f 2 -d ''='');\
	echo Generate private key;\
	echo $$jwt_passphrase | openssl genpkey -out config/jwt/private.pem -pass stdin -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096;\
	echo Generate public key;\
	echo $$jwt_passphrase | openssl pkey -in config/jwt/private.pem -passin stdin -out config/jwt/public.pem -pubout;
	@if [ ! -f config/jwt/private-test.pem ]; \
	then\
		jwt_passphrase=$$(grep ''^JWT_PASSPHRASE='' .env.test.local | cut -f 2 -d ''='');\
		echo Generate private key;\
		echo $$jwt_passphrase | openssl genpkey -out config/jwt/private-test.pem -pass stdin -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096;\
		echo Generate public key;\
		echo $$jwt_passphrase | openssl pkey -in config/jwt/private-test.pem -passin stdin -out config/jwt/public-test.pem -pubout;\
	fi

.PHONY: auth

##
## Quality assurance
## -----------------
##

quality: .php_cs .phpmd.xml .phpcs.xml phpstan.neon psalm.xml phpunit.xml ## All test of quality with grumphp (phpcsfixer, phpcpd...)
	$(SYMFONY_BIN) php vendor/bin/grumphp run -n

.php_cs: .php_cs.dist
	@if [ -f .php_cs ]; \
	then\
		echo '\033[1;41m/!\ The .php_cs.dist file has changed. Please check your .php_cs file (this message will not be displayed again).\033[0m';\
		touch .php_cs;\
	else\
		echo cp .php_cs.dist .php_cs;\
		cp .php_cs.dist .php_cs;\
	fi

.phpmd.xml: .phpmd.xml.dist
	@if [ -f .phpmd.xml ]; \
	then\
		echo '\033[1;41m/!\ The .phpmd.xml.dist file has changed. Please check your .phpmd.xml file (this message will not be displayed again).\033[0m';\
		touch .phpmd.xml;\
	else\
		echo cp .phpmd.xml.dist .phpmd.xml;\
		cp .phpmd.xml.dist .phpmd.xml;\
	fi

.phpcs.xml: .phpcs.xml.dist
	@if [ -f .phpcs.xml ]; \
	then\
		echo '\033[1;41m/!\ The .phpcs.xml.dist file has changed. Please check your .phpcs.xml file (this message will not be displayed again).\033[0m';\
		touch .phpcs.xml;\
	else\
		echo cp .phpcs.xml.dist .phpcs.xml;\
		cp .phpcs.xml.dist .phpcs.xml;\
	fi

phpstan.neon: phpstan.neon.dist
	@if [ -f phpstan.neon ]; \
	then\
		echo '\033[1;41m/!\ The phpstan.neon.dist file has changed. Please check your phpstan.neon file (this message will not be displayed again).\033[0m';\
		touch phpstan.neon;\
	else\
		echo cp phpstan.neon.dist phpstan.neon;\
		cp phpstan.neon.dist phpstan.neon;\
	fi

psalm.xml: psalm.xml.dist
	@if [ -f psalm.xml ]; \
	then\
		echo '\033[1;41m/!\ The psalm.xml.dist file has changed. Please check your psalm.xml file (this message will not be displayed again).\033[0m';\
		touch psalm.xml;\
	else\
		echo cp psalm.xml.dist psalm.xml;\
		cp psalm.xml.dist psalm.xml;\
	fi

phpunit.xml: phpunit.xml.dist
	@if [ -f phpunit.xml ]; \
	then\
		echo '\033[1;41m/!\ The phpunit.xml.dist file has changed. Please check your phpunit.xml file (this message will not be displayed again).\033[0m';\
		touch phpunit.xml;\
	else\
		echo cp phpunit.xml.dist phpunit.xml;\
		cp phpunit.xml.dist phpunit.xml;\
	fi

.PHONY: quality2


